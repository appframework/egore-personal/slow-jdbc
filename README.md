Delaying JDBC driver for JDK 11
===============================

Rationale
---------

This driver can be used to artificially slow down SQL communication between an application and a database server.
It adds a constant delay to JDBC calls (by default 100ms). This is meant to identify database related performance issues
during development. It is especially handy to identify non-optimized usage of ORMs like Hibernate, which lazily loads
data possibly transparent to the developer.

Usage
----- 

This driver requires at least JDK 11 plus a real JDBC driver. It use the driver replace your JDBC connection URL by
inserting "slow:" in it. For example:

    "jdbc:hsqldb:mem:unittest"
    
becomes

    "jdbc:slow:hsqldb:mem:unittest"

To adjust the delay added to the calls grab the de.egore911.slowjdbc.Delay class from the appropriate classloader and
modify the AMOUNT value to the milliseconds you desire.

Usage of XA in JBoss EAP 7 (Wildfly)
------------------------------------

Create a module within your JBoss

    <module xmlns="urn:jboss:module:1.1" name="de.egore911.slowjdbc">
      <resources>
        <resource-root path="slow-jdbc-1.0.0.jar"/>
      </resources>
      <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
      </dependencies>
    </module>

Register your JDBC driver and XA driver.

    <driver name="slow" module="de.egore911.slowjdbc">
      <driver-class>de.egore911.slowjdbc.SlowDriver</driver-class>
      <xa-datasource-class>de.egore911.slowjdbc.SlowXADataSource</xa-datasource-class>
    </driver>

Create a XA datasource for e.g. PostgreSQL

    <xa-datasource jndi-name="java:/datasources/SomeDataSource" pool-name="SomePoolName">
      <xa-datasource-property name="DelegateClassName">org.postgresql.xa.PGXADataSource</xa-datasource-property>
      <xa-datasource-property name="ServerName">localhost</xa-datasource-property>
      <xa-datasource-property name="PortNumber">1433</xa-datasource-property>
      <xa-datasource-property name="DatabaseName">MyDatabase</xa-datasource-property>
      <driver>slow</driver>
      <security>
        <user-name>DatabaseUser</user-name>
        <password>DatabasePassword</password>
      </security>
    </xa-datasource>

Disclaimer
----------

The author of the driver has minimal insight into JDBC driver development. Do not use this driver in production. Send
pull requests if you find bugs or know improvements.
