package de.egore911.slowjdbc;

public class Delay {

    public static int AMOUNT = 100;

    public static void slowdown() {
        try {
            Thread.sleep(AMOUNT);
        } catch (InterruptedException ignore) {
        }
    }
}
