package de.egore911.slowjdbc;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

public class SlowXAResource implements XAResource {

    private final XAResource delegate;

    public SlowXAResource(XAResource delegate) {
        this.delegate = delegate;
    }

    @Override
    public void commit(Xid xid, boolean b) throws XAException {
        Delay.slowdown();
        delegate.commit(xid, b);
    }

    @Override
    public void end(Xid xid, int i) throws XAException {
        Delay.slowdown();
        delegate.end(xid, i);
    }

    @Override
    public void forget(Xid xid) throws XAException {
        Delay.slowdown();
        delegate.forget(xid);
    }

    @Override
    public int getTransactionTimeout() throws XAException {
        return delegate.getTransactionTimeout();
    }

    @Override
    public boolean isSameRM(XAResource xaResource) throws XAException {
        return delegate.isSameRM(xaResource);
    }

    @Override
    public int prepare(Xid xid) throws XAException {
        Delay.slowdown();
        return delegate.prepare(xid);
    }

    @Override
    public Xid[] recover(int i) throws XAException {
        return delegate.recover(i);
    }

    @Override
    public void rollback(Xid xid) throws XAException {
        Delay.slowdown();
        delegate.rollback(xid);
    }

    @Override
    public boolean setTransactionTimeout(int i) throws XAException {
        return delegate.setTransactionTimeout(i);
    }

    @Override
    public void start(Xid xid, int i) throws XAException {
        Delay.slowdown();
        delegate.start(xid, i);
    }
}
