package de.egore911.slowjdbc;

import javax.sql.ConnectionEventListener;
import javax.sql.StatementEventListener;
import javax.sql.XAConnection;
import javax.transaction.xa.XAResource;
import java.sql.Connection;
import java.sql.SQLException;

public class SlowXAConnection implements XAConnection {

    private final XAConnection delegate;

    private SlowXAResource wrappedResource;

    public SlowXAConnection(XAConnection delegate) {
        this.delegate = delegate;
    }

    @Override
    public XAResource getXAResource() throws SQLException {
        // Needs to return the same instance each time
        if (wrappedResource == null) {
            synchronized (this) {
                if (wrappedResource == null) {
                    wrappedResource = new SlowXAResource(delegate.getXAResource());
                }
            }
        }

        return wrappedResource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return new SlowConnection(delegate.getConnection());
    }

    @Override
    public void close() throws SQLException {
        delegate.close();
    }

    @Override
    public void addConnectionEventListener(ConnectionEventListener listener) {
        delegate.addConnectionEventListener(listener);
    }

    @Override
    public void removeConnectionEventListener(ConnectionEventListener listener) {
        delegate.removeConnectionEventListener(listener);
    }

    @Override
    public void addStatementEventListener(StatementEventListener listener) {
        delegate.addStatementEventListener(listener);
    }

    @Override
    public void removeStatementEventListener(StatementEventListener listener) {
        delegate.removeStatementEventListener(listener);
    }
}
