package de.egore911.slowjdbc;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SlowDriver implements Driver {

    public static SlowDriver instance;
    private static int major = 0;
    private static int minor = 1;

    static {
        System.err.println("Driver is loaded");
        try {
            instance = new SlowDriver();
            DriverManager.registerDriver(instance);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String path = "META-INF/maven/de.egore911/slow-jdbc/pom.properties";
        try (InputStream in = SlowDriver.class.getClassLoader().getResourceAsStream(path)) {
            if (in != null) {
                Properties properties = new Properties();
                properties.load(in);

                String v = properties.getProperty("version");
                if (v != null && !v.isEmpty()) {
                    Pattern p = Pattern.compile("(\\d+)\\.(\\d+)\\..*");
                    Matcher matcher = p.matcher(v);
                    if (matcher.matches()) {
                        major = Integer.parseInt(matcher.group(1));
                        minor = Integer.parseInt(matcher.group(2));
                    }
                }
            }
        } catch (Exception ignored) {
        }
    }

    public SlowDriver() {
    }

    public Connection connect(String url, Properties info) throws SQLException {
        if (acceptsURL(url)) {
            return new SlowConnection(DriverManager.getConnection(url.replace("jdbc:slow:", "jdbc:"), info));
        }
        return null;
    }

    public boolean acceptsURL(String url) {

        if (url == null || url.isEmpty()) {
            return false;
        }

        if (url.regionMatches(true, 0, "jdbc:slow:", 0, "jdbc:slow:".length())) {
            return true;
        }

        return false;
    }

    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        throw new NoSuchMethodError();
    }

    public int getMajorVersion() {
        return major;
    }

    public int getMinorVersion() {
        return minor;
    }

    public boolean jdbcCompliant() {
        return true;
    }

    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new NoSuchMethodError();
    }
}
