package de.egore911.slowjdbc;

import javax.sql.XAConnection;
import javax.sql.XAConnectionBuilder;
import javax.sql.XADataSource;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.ShardingKeyBuilder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Usage in JBoss EAP 7
 *
 * <driver name="slow" module="de.egore911.slowjdbc">
 *   <driver-class>de.egore911.slowjdbc.SlowDriver</driver-class>
 *   <xa-datasource-class>de.egore911.slowjdbc.SlowXADataSource</xa-datasource-class>
 * </driver>
 *
 * For MS SQL
 * <xa-datasource jndi-name="java:/datasources/SomeDataSource" pool-name="SomePoolName">
 *   <xa-datasource-property name="DelegateClassName">com.microsoft.sqlserver.jdbc.SQLServerXADataSource</xa-datasource-property>
 *   <xa-datasource-property name="ServerName">localhost</xa-datasource-property>
 *   <xa-datasource-property name="PortNumber">1433</xa-datasource-property>
 *   <xa-datasource-property name="DatabaseName">MyDatabase</xa-datasource-property>
 *   <driver>slow</driver>
 *   <security>
 *     <user-name>DatabaseUser</user-name>
 *     <password>DatabasePassword</password>
 *   </security>
 * </xa-datasource>
 *
 * For PostgreSQL
 * <xa-datasource jndi-name="java:/datasources/SomeDataSource" pool-name="SomePoolName">
 *   <xa-datasource-property name="DelegateClassName">org.postgresql.xa.PGXADataSource</xa-datasource-property>
 *   <xa-datasource-property name="ServerName">localhost</xa-datasource-property>
 *   <xa-datasource-property name="PortNumber">1433</xa-datasource-property>
 *   <xa-datasource-property name="DatabaseName">MyDatabase</xa-datasource-property>
 *   <driver>slow</driver>
 *   <security>
 *     <user-name>DatabaseUser</user-name>
 *     <password>DatabasePassword</password>
 *   </security>
 * </xa-datasource>
 *
 * Supported properties
 * <ul>
 *     <li>DelegateClassName</li>
 *     <li>ServerName</li>
 *     <li>PortNumber</li>
 *     <li>DatabaseName</li>
 * </ul>
 */
public class SlowXADataSource implements XADataSource {

    private XADataSource delegate;

    private String delegateClassName;
    private final Map<String, Object> valueStore = new HashMap<>();

    private XADataSource getDelegate() throws SQLException {
        if (delegate == null) {
            synchronized (this) {
                if (delegate == null) {
                    try {
                        delegate = (XADataSource) Class.forName(delegateClassName).getConstructor().newInstance();
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
                        throw new SQLException("Failed to initialize delegate XA class", e);
                    }
                    for (Map.Entry<String, Object> element : valueStore.entrySet()) {
                        setValue(element.getKey(), element.getValue());
                    }
                }
            }
        }
        return delegate;
    }

    private void setValue(String key, Object value) throws SQLException {
        try {
            Method method = delegate.getClass().getMethod("set" + toUpperFirst(key), value.getClass());
            method.invoke(delegate, value);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new SQLException("Failed to initialize delegate XA class", e);
        }
    }

    private String toUpperFirst(String value) {
        if (value == null) {
            throw new RuntimeException("Key must not be null");
        }
        if (value.length() < 2) {
            throw new RuntimeException("Key must be at least two chars long");
        }
        return value.substring(0, 1).toUpperCase() + value.substring(1);
    }

    @Override
    public XAConnection getXAConnection() throws SQLException {
        return new SlowXAConnection(getDelegate().getXAConnection());
    }

    @Override
    public XAConnection getXAConnection(String user, String password) throws SQLException {
        return new SlowXAConnection(getDelegate().getXAConnection(user, password));
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return getDelegate().getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        getDelegate().setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        getDelegate().setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return getDelegate().getLoginTimeout();
    }

    @Override
    public XAConnectionBuilder createXAConnectionBuilder() throws SQLException {
        return getDelegate().createXAConnectionBuilder();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        try {
            return getDelegate().getParentLogger();
        } catch (SQLException e) {
            throw new SQLFeatureNotSupportedException(e.getMessage(), e);
        }
    }

    @Override
    public ShardingKeyBuilder createShardingKeyBuilder() throws SQLException {
        return getDelegate().createShardingKeyBuilder();
    }

    // XA properties set via reflection

    public String getDelegateClassName() {
        return delegateClassName;
    }

    public void setDelegateClassName(String delegateClassName) {
        this.delegateClassName = delegateClassName;
    }

    /**
     * Put value in store of cached values and set it on delegate if possible. This allows "DelegateClassName" not to
     * be set first, but will apply them in {@link #getDelegate()} later on.
     *
     * @param key key to set (e.g. "DatabaseName" when called via {@link #setDatabaseName(String)})
     * @param value value to set (e.g. "MyDatabase" in the example mentioned in classes javadoc)
     * @throws SQLException thrown if setting the value on the delegate failed (e.g. if the method did not exist on the
     * delegate)
     */
    private void putAndSetValue(String key, Object value) throws SQLException {
        valueStore.put(key, value);
        if (delegate != null) {
            setValue(key, value);
        }
    }

    public String getServerName() {
        return (String) valueStore.get("ServerName");
    }

    /**
     * Set server name
     */
    public void setServerName(String serverName) throws SQLException {
        putAndSetValue("ServerName", serverName);
    }

    public int getPortNumber() {
        return (int) valueStore.get("PortNumber");
    }

    /**
     * Set port number
     */
    public void setPortNumber(int portNumber) throws SQLException {
        putAndSetValue("PortNumber", portNumber);
    }

    public String getDatabaseName() {
        return (String) valueStore.get("DatabaseName");
    }
    /**
     * Set database name
     */
    public void setDatabaseName(String databaseName) throws SQLException {
        putAndSetValue("DatabaseName", databaseName);
    }

}
