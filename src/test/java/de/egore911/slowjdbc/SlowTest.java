package de.egore911.slowjdbc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SlowTest {

    @Test
    public void testConnect() throws SQLException, ClassNotFoundException {
        Class.forName("de.egore911.slowjdbc.SlowDriver");

        Connection connection = DriverManager.getConnection("jdbc:hsqldb:mem:unittest", "SA", "");
        connection.createStatement().execute("CREATE TABLE dummy (id INT NOT NULL, PRIMARY KEY (id))");
        connection.createStatement().execute("INSERT INTO dummy (id) VALUES (1)");

        Connection slowConnection = DriverManager.getConnection("jdbc:slow:hsqldb:mem:unittest", "SA", "");
        Assertions.assertTrue(slowConnection instanceof SlowConnection);

        slowConnection.createStatement().execute("INSERT INTO dummy (id) VALUES (2)");

        Statement statement = slowConnection.createStatement();
        Assertions.assertTrue(statement instanceof SlowStatement);
        long start = System.currentTimeMillis();
        ResultSet result = statement.executeQuery("SELECT id FROM dummy ORDER BY id");
        Assertions.assertTrue(result.next());
        Assertions.assertEquals(result.getInt(1), 1);
        Assertions.assertTrue(result.next());
        Assertions.assertEquals(result.getInt(1), 2);
        long delta = System.currentTimeMillis() - start;
        Assertions.assertTrue(delta > Delay.AMOUNT - 100);
        Assertions.assertTrue(delta < Delay.AMOUNT + 100);

    }
}
